﻿using BugFixRocks.Models;
using System;
using System.Data.Entity;

namespace BugFixRocks.Repository.Contexts
{
    public interface IBugFixRocksContext
    {
        DbSet<AspNetRole> AspNetRoles { get; set; }
        DbSet<AspNetUserClaim> AspNetUserClaims { get; set; }
        DbSet<AspNetUserLogin> AspNetUserLogins { get; set; }
        DbSet<AspNetUser> AspNetUsers { get; set; }
        DbSet<Attachment> Attachments { get; set; }
        DbSet<EntryType> EntryTypes { get; set; }
        DbSet<Issue> Issues { get; set; }
        DbSet<Priority> Priorities { get; set; }
        DbSet<ProjectMember> ProjectMembers { get; set; }
        DbSet<Project> Projects { get; set; }
        DbSet<Resolution> Resolutions { get; set; }
        DbSet<ResolutionType> ResolutionTypes { get; set; }
        DbSet<Tag> Tags { get; set; }
        DbSet<TeamMember> TeamMembers { get; set; }
        DbSet<Team> Teams { get; set; }
        DbSet<Versioning> Versionings { get; set; }
    }
}
