﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using BugFixRocks.Models;
using BugFixRocks.Repository.Contexts;

namespace BugFixRocks.Repository
{
    /// <summary>
    /// Use only for scaffolding
    /// </summary>
    public class ScaffoldingDBContext : DbContext, IBugFixRocksContext
    {
        public DbSet<ProjectMember> ProjectMembers { get; set; }
        public DbSet<Project> Projects { get; set; }
        public DbSet<Tag> Tags { get; set; }
        public DbSet<Team> Teams { get; set; }
        public DbSet<TeamMember> TeamMembers { get; set; }
        public DbSet<Versioning> Versionings { get; set; }
        public DbSet<AspNetRole> AspNetRoles { get; set; }
        public DbSet<AspNetUserClaim> AspNetUserClaims { get; set; }
        public DbSet<AspNetUserLogin> AspNetUserLogins { get; set; }
        public DbSet<AspNetUser> AspNetUsers { get; set; }
        public DbSet<Attachment> Attachments { get; set; }
        public DbSet<EntryType> EntryTypes { get; set; }
        public DbSet<Issue> Issues { get; set; }
        public DbSet<Priority> Priorities { get; set; }
        public DbSet<Resolution> Resolutions { get; set; }
        public DbSet<ResolutionType> ResolutionTypes { get; set; }
    }
}
