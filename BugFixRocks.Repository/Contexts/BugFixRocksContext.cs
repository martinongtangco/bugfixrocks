using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using Repository.Pattern.Ef6;
using BugFixRocks.Repository.Mapping;
using BugFixRocks.Models;
using BugFixRocks.Repository.Contexts;

namespace BugFixRocks.Repository
{
    public partial class BugFixRocksContext : DataContext, IBugFixRocksContext
    {
        static BugFixRocksContext()
        {
            Database.SetInitializer<BugFixRocksContext>(null);
        }

        public BugFixRocksContext()
            : base("Name=MartinOngtangcoDB")
        {
            this.Configuration.LazyLoadingEnabled = true;
        }

        public DbSet<ProjectMember> ProjectMembers { get; set; }
        public DbSet<Project> Projects { get; set; }
        public DbSet<Tag> Tags { get; set; }
        public DbSet<Team> Teams { get; set; }
        public DbSet<TeamMember> TeamMembers { get; set; }
        public DbSet<Versioning> Versionings { get; set; }
        public DbSet<AspNetRole> AspNetRoles { get; set; }
        public DbSet<AspNetUserClaim> AspNetUserClaims { get; set; }
        public DbSet<AspNetUserLogin> AspNetUserLogins { get; set; }
        public DbSet<AspNetUser> AspNetUsers { get; set; }
        public DbSet<Attachment> Attachments { get; set; }
        public DbSet<EntryType> EntryTypes { get; set; }
        public DbSet<Issue> Issues { get; set; }
        public DbSet<Priority> Priorities { get; set; }
        public DbSet<Resolution> Resolutions { get; set; }
        public DbSet<ResolutionType> ResolutionTypes { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Configurations.Add(new ProjectMemberMap());
            modelBuilder.Configurations.Add(new ProjectMap());
            modelBuilder.Configurations.Add(new TagMap());
            modelBuilder.Configurations.Add(new TeamMap());
            modelBuilder.Configurations.Add(new TeamMemberMap());
            modelBuilder.Configurations.Add(new VersioningMap());
            modelBuilder.Configurations.Add(new AspNetRoleMap());
            modelBuilder.Configurations.Add(new AspNetUserClaimMap());
            modelBuilder.Configurations.Add(new AspNetUserLoginMap());
            modelBuilder.Configurations.Add(new AspNetUserMap());
            modelBuilder.Configurations.Add(new AttachmentMap());
            modelBuilder.Configurations.Add(new EntryTypeMap());
            modelBuilder.Configurations.Add(new IssueMap());
            modelBuilder.Configurations.Add(new PriorityMap());
            modelBuilder.Configurations.Add(new ResolutionMap());
            modelBuilder.Configurations.Add(new ResolutionTypeMap());
        }
    }
}
