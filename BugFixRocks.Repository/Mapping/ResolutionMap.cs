using BugFixRocks.Models;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace BugFixRocks.Repository.Mapping
{
    public class ResolutionMap : EntityTypeConfiguration<Resolution>
    {
        public ResolutionMap()
        {
            // Primary Key
            this.HasKey(t => t.Id);

            // Properties
            // Table & Column Mappings
            this.ToTable("Resolution", "Entries");
            this.Property(t => t.Id).HasColumnName("Id");
            this.Property(t => t.IssueId).HasColumnName("IssueId");
            this.Property(t => t.ResolutionType).HasColumnName("ResolutionType");
            this.Property(t => t.Comment).HasColumnName("Comment");
            this.Property(t => t.UserId).HasColumnName("UserId");
            this.Property(t => t.CreatedUTC).HasColumnName("CreatedUTC");

            // Relationships
            this.HasRequired(t => t.Issue)
                .WithMany(t => t.Resolutions)
                .HasForeignKey(d => d.IssueId);
            this.HasRequired(t => t.ResolutionType1)
                .WithMany(t => t.Resolutions)
                .HasForeignKey(d => d.ResolutionType);

        }
    }
}
