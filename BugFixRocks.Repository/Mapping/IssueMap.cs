using BugFixRocks.Models;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace BugFixRocks.Repository.Mapping
{
    public class IssueMap : EntityTypeConfiguration<Issue>
    {
        public IssueMap()
        {
            // Primary Key
            this.HasKey(t => t.Id);

            // Properties
            this.Property(t => t.Title)
                .IsRequired()
                .HasMaxLength(200);

            // Table & Column Mappings
            this.ToTable("Issues", "Entries");
            this.Property(t => t.Id).HasColumnName("Id");
            this.Property(t => t.ProjectId).HasColumnName("ProjectId");
            this.Property(t => t.VersionId).HasColumnName("VersionId");
            this.Property(t => t.Title).HasColumnName("Title");
            this.Property(t => t.Description).HasColumnName("Description");
            this.Property(t => t.PriorityId).HasColumnName("PriorityId");
            this.Property(t => t.ResolutionTypeId).HasColumnName("ResolutionTypeId");
            this.Property(t => t.AssigneeId).HasColumnName("AssigneeId");
            this.Property(t => t.ReporterId).HasColumnName("ReporterId");
            this.Property(t => t.CreatedUTC).HasColumnName("CreatedUTC");
            this.Property(t => t.ModifiedUTC).HasColumnName("ModifiedUTC");
            this.Property(t => t.Tags).HasColumnName("Tags");

            // Relationships
            this.HasRequired(t => t.Project)
                .WithMany(t => t.Issues)
                .HasForeignKey(d => d.ProjectId);
            this.HasRequired(t => t.Priority)
                .WithMany(t => t.Issues)
                .HasForeignKey(d => d.PriorityId);
            this.HasRequired(t => t.ResolutionType)
                .WithMany(t => t.Issues)
                .HasForeignKey(d => d.ResolutionTypeId);

        }
    }
}
