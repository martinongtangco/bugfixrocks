using BugFixRocks.Models;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace BugFixRocks.Repository.Mapping
{
    public class ProjectMemberMap : EntityTypeConfiguration<ProjectMember>
    {
        public ProjectMemberMap()
        {
            // Primary Key
            this.HasKey(t => t.Id);

            // Properties
            this.Property(t => t.NotifyDays)
                .IsRequired()
                .HasMaxLength(20);

            // Table & Column Mappings
            this.ToTable("ProjectMembers", "Agile");
            this.Property(t => t.Id).HasColumnName("Id");
            this.Property(t => t.ProjectId).HasColumnName("ProjectId");
            this.Property(t => t.UserId).HasColumnName("UserId");
            this.Property(t => t.NotifyUpdates).HasColumnName("NotifyUpdates");
            this.Property(t => t.NotifyDays).HasColumnName("NotifyDays");
            this.Property(t => t.NotifyStartHour).HasColumnName("NotifyStartHour");
            this.Property(t => t.NotifyEndHour).HasColumnName("NotifyEndHour");
            this.Property(t => t.Timezone).HasColumnName("Timezone");
            this.Property(t => t.CreatedUTC).HasColumnName("CreatedUTC");
            this.Property(t => t.ModifiedUTC).HasColumnName("ModifiedUTC");

            // Relationships
            this.HasRequired(t => t.Project)
                .WithMany(t => t.ProjectMembers)
                .HasForeignKey(d => d.ProjectId);

        }
    }
}
