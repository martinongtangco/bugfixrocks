using BugFixRocks.Models;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace BugFixRocks.Repository.Mapping
{
    public class VersioningMap : EntityTypeConfiguration<Versioning>
    {
        public VersioningMap()
        {
            // Primary Key
            this.HasKey(t => t.Id);

            // Properties
            this.Property(t => t.Version)
                .IsRequired()
                .HasMaxLength(1000);

            // Table & Column Mappings
            this.ToTable("Versioning", "Agile");
            this.Property(t => t.Id).HasColumnName("Id");
            this.Property(t => t.ProjectId).HasColumnName("ProjectId");
            this.Property(t => t.Version).HasColumnName("Version");
            this.Property(t => t.CreatedUTC).HasColumnName("CreatedUTC");
            this.Property(t => t.ModifiedUTC).HasColumnName("ModifiedUTC");
            this.Property(t => t.CreatorId).HasColumnName("CreatorId");

            // Relationships
            this.HasRequired(t => t.Project)
                .WithMany(t => t.Versionings)
                .HasForeignKey(d => d.ProjectId);

        }
    }
}
