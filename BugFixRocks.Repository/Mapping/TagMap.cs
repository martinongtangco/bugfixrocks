using BugFixRocks.Models;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace BugFixRocks.Repository.Mapping
{
    public class TagMap : EntityTypeConfiguration<Tag>
    {
        public TagMap()
        {
            // Primary Key
            this.HasKey(t => t.Id);

            // Properties
            this.Property(t => t.Id)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.Tag1)
                .IsRequired()
                .HasMaxLength(100);

            // Table & Column Mappings
            this.ToTable("Tags", "Agile");
            this.Property(t => t.Id).HasColumnName("Id");
            this.Property(t => t.TeamId).HasColumnName("TeamId");
            this.Property(t => t.Tag1).HasColumnName("Tag");

            // Relationships
            this.HasRequired(t => t.Team)
                .WithMany(t => t.Tags)
                .HasForeignKey(d => d.TeamId);

        }
    }
}
