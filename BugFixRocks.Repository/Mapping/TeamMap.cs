using BugFixRocks.Models;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace BugFixRocks.Repository.Mapping
{
    public class TeamMap : EntityTypeConfiguration<Team>
    {
        public TeamMap()
        {
            // Primary Key
            this.HasKey(t => t.Id);

            // Properties
            this.Property(t => t.Name)
                .IsRequired()
                .HasMaxLength(100);

            this.Property(t => t.Description)
                .HasMaxLength(1000);

            this.Property(t => t.Email)
                .IsRequired()
                .HasMaxLength(2000);

            this.Property(t => t.Website)
                .HasMaxLength(2000);

            // Table & Column Mappings
            this.ToTable("Team", "Agile");
            this.Property(t => t.Id).HasColumnName("Id");
            this.Property(t => t.Name).HasColumnName("Name");
            this.Property(t => t.LogoBase64).HasColumnName("LogoBase64");
            this.Property(t => t.Description).HasColumnName("Description");
            this.Property(t => t.CreatorId).HasColumnName("CreatorId");
            this.Property(t => t.CreatedUTC).HasColumnName("CreatedUTC");
            this.Property(t => t.ModifiedUTC).HasColumnName("ModifiedUTC");
            this.Property(t => t.IsPublic).HasColumnName("IsPublic");
            this.Property(t => t.Email).HasColumnName("Email");
            this.Property(t => t.Website).HasColumnName("Website");
            this.Property(t => t.EmailGroup).HasColumnName("EmailGroup");
            this.Property(t => t.Timezone).HasColumnName("Timezone");

            // Relationships
            this.HasRequired(t => t.Creator)
                .WithMany(t => t.Teams)
                .HasForeignKey(d => d.CreatorId)
                .WillCascadeOnDelete(false);
        }
    }
}
