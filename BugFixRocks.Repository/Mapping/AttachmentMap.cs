using BugFixRocks.Models;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace BugFixRocks.Repository.Mapping
{
    public class AttachmentMap : EntityTypeConfiguration<Attachment>
    {
        public AttachmentMap()
        {
            // Primary Key
            this.HasKey(t => new { t.IssueId, t.AttachmentURL });

            // Properties
            this.Property(t => t.IssueId)
                .HasDatabaseGeneratedOption(DatabaseGeneratedOption.None);

            this.Property(t => t.AttachmentURL)
                .IsRequired()
                .HasMaxLength(3000);

            // Table & Column Mappings
            this.ToTable("Attachments", "Entries");
            this.Property(t => t.IssueId).HasColumnName("IssueId");
            this.Property(t => t.AttachmentURL).HasColumnName("AttachmentURL");

            // Relationships
            this.HasRequired(t => t.Issue)
                .WithMany(t => t.Attachments)
                .HasForeignKey(d => d.IssueId);

        }
    }
}
