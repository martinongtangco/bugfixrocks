using BugFixRocks.Models;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;

namespace BugFixRocks.Repository.Mapping
{
    public class ProjectMap : EntityTypeConfiguration<Project>
    {
        public ProjectMap()
        {
            // Primary Key
            this.HasKey(t => t.Id);

            // Properties
            this.Property(t => t.Name)
                .IsRequired()
                .HasMaxLength(100);

            // Table & Column Mappings
            this.ToTable("Projects", "Agile");
            this.Property(t => t.Id).HasColumnName("Id");
            this.Property(t => t.Name).HasColumnName("Name");
            this.Property(t => t.CreatedUTC).HasColumnName("CreatedUTC");
            this.Property(t => t.ModifiedUTC).HasColumnName("ModifiedUTC");
            this.Property(t => t.CreatorId).HasColumnName("CreatorId");
            this.Property(t => t.LogoBase64).HasColumnName("LogoBase64");
        }
    }
}
