﻿var bugFixRocksApp = angular.module("bugFixRocksApp",
                                    [
                                        // dependencies
                                        "ngRoute",
                                        "ui.bootstrap"
                                    ]);

bugFixRocksApp.config(
    function ($routeProvider, $locationProvider) {
        $routeProvider
            .when("/home", {
                templateUrl: "app/Home.html",
                controller: "HomeController"
            })
            .when("/newProjectForm", {
                templateUrl: "app/Project/ProjectFormView.html",
                controller: "ProjectController"
            })
            .when("/editProjectForm/:id", {
                templateUrl: "app/Project/ProjectFormView.html",
                controller: "ProjectController"
            })
        .otherwise({
            redirectTo: "/home"
        });

        $locationProvider.html5Mode(true);
    });

bugFixRocksApp.controller("HomeController",
    function ($scope, $location, $modal, DataService) {

        $scope.addNewProject = function () {
            $modal.open({
                templateUrl: "app/Project/ProjectFormView.html",
                controller: "ProjectController"
            });
            //$location.path('/newProjectForm');
        };

        $scope.editProject = function (id) {
            $location.path('/editProjectForm/' + id);
        };
    });