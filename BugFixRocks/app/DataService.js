﻿bugFixRocksApp.factory("DataService",
    function () {
        var getProject = function (id) {
            if (id == 123) {
                return {
                    Id: 123,
                    Name: "Project 123"
                };
            }

            return undefined;
        };

        var insertProject = function (newProject) {
            return true;
        };

        var updateProject = function (newProject) {
            return true;
        };

        return {
            getProject: getProject,
            insertProject: insertProject,
            updateProject: updateProject
        };
    });