﻿bugFixRocksApp.controller("ProjectController",
    function ($scope, $location, $routeParams, $modalInstance, DataService) {

        if ($routeParams.id)
            $scope.project = DataService.getProject($routeParams.id);
        else
            $scope.project = {
                Id: 0
            };

        $scope.editableProject = angular.copy($scope.project);

        $scope.cancel = function () {
            //$window.history.back();
            $modalInstance.dismiss();
        };

        $scope.shouldShowFullName = function () {
            return false;
        };

        $scope.submit = function () {
            if ($scope.editableProject.id == 0) {
                // insert
                DataService.insertProject($scope.editableProject);
            } else {
                // update
                DataService.updateProject($scope.editableProject);
            }
            
            $scope.project = angular.copy($scope.editableProject);
            $modalInstance.close();
            //$window.history.back();
        };
    });