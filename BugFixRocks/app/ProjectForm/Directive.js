﻿bugFixRocksApp.directive("projectForm",
    function () {
        return {
            restrict: 'E',
            templateUrl: 'app/ProjectForm/ProjectFormView.html'
        };
    });