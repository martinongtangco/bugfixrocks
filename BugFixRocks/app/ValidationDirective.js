﻿

bugFixRocksApp.directive('showErrors', function () {
    return {
        restrict: 'A',
        require: '^form',
        link: function (scope, el, attrs, formCtrl) {

            // find the textbox element, which has the 'name' attribute
            var inputElement = el[0].querySelector('[name]');

            // convert the native text box element to an angular element
            var inputNgElement = angular.element(inputElement);

            // get the name on the text box so we know the property to check on the form controller
            var inputName = inputNgElement.attr('name');

            var helpText = angular.element(el[0].querySelector('.help-block'));

            // only the has-error class after the user leaves the textbox
            inputNgElement.bind('blur', function () {
                el.toggleClass('has-error', formCtrl[inputName].$invalid);
                helpText.toggleClass('hide', formCtrl[inputName].$valid);
            });
        }
    };
});