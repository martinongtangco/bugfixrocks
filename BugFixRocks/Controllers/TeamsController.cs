﻿using System;
using System.Threading.Tasks;
using System.Web.Mvc;
using System.Net;
using System.Data.Entity;
using System.Linq;
using System.Collections.ObjectModel;

using Microsoft.AspNet.Identity;

using Repository.Pattern.Repositories;
using Repository.Pattern.UnitOfWork;
using Repository.Pattern.Infrastructure;

using BugFixRocks.Models;
using BugFixRocks.Repository;
using BugFixRocks.Services.Interfaces;

namespace BugFixRocks.Web.Controllers
{
    [Authorize]
    public class TeamsController : Controller
    {
        private readonly IUnitOfWorkAsync _uow;
        private readonly ITeamsService _service;

        public TeamsController(IUnitOfWorkAsync uow, ITeamsService service)
        {
            _uow = uow;
            _service = service;
        }

        // GET: Teams
        public async Task<ViewResult> Index()
        {
            return View(await _service.GetAllTeams());
        }

        // GET: Teams/Details/5
        public async Task<ActionResult> Details(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            Team team = await _service.FindAsync(id);
            if (team == null)
            {
                return HttpNotFound();
            }

            SetViewBagTimeZoneOffsetList(team.Timezone);

            return View(team);
        }

        // GET: Teams/Create
        public ActionResult Create()
        {
            SetViewBagTimeZoneOffsetList(8);

            return View(new Team { Timezone = 8 });
        }

        // POST: Teams/Create
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Create([Bind(Include = "Name,LogoBase64,Description,Email,Website,Timezone")] 
                                               Team team)
        {
            if (!ModelState.IsValid)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            int userId = User.Identity.GetUserId<int>();

            try
            {
                _uow.BeginTransaction();

                _service.Insert(team, userId);
                await _uow.SaveChangesAsync();

                _uow.Commit();
            }
            catch
            {
                _uow.Rollback();
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            return RedirectToAction("Index");
        }

        // GET: Teams/Edit/5
        public async Task<ActionResult> Edit(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            Team team = await _service.FindAsync(id);
            if (team == null)
            {
                return HttpNotFound();
            }

            ViewBag.DisplayImage = string.IsNullOrEmpty(team.LogoBase64) ? "display:none;" : string.Empty;
            SetViewBagTimeZoneOffsetList(team.Timezone);

            return View(team);
        }

        // POST: Teams/Edit/5
        // To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        // more details see http://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Edit([Bind(Include = "Id,Name,LogoBase64,Description,Email,Website,Timezone")] 
                                             Team team)
        {
            if (!ModelState.IsValid)
            {
                return View(team);
            }

            try
            {
                _uow.BeginTransaction();

                await _service.Update(team);
                await _uow.SaveChangesAsync();
                
                _uow.Commit();
            }
            catch
            {
                _uow.Rollback();
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            return RedirectToAction("Index");
        }

        // GET: Teams/Delete/5
        public async Task<ActionResult> Delete(int? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            Team team = await _service.FindAsync(id);
            if (team == null)
            {
                return HttpNotFound();
            }

            return View(team);
        }

        // POST: Teams/Delete/5
        [HttpPost, ActionName("Delete")]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> DeleteConfirmed(int id)
        {
            try
            {
                _uow.BeginTransaction();

                _service.Delete(id);
                await _uow.SaveChangesAsync();

                _uow.Commit();
            }
            catch
            {
                _uow.Rollback();
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }

            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                _uow.Dispose();
            }
            base.Dispose(disposing);
        }

        private void SetViewBagTimeZoneOffsetList(int offset)
        {
            var tz = TimeZoneInfo.GetSystemTimeZones()
                                 .Select(t => new { Id = t.BaseUtcOffset.Hours, Name = t.DisplayName })
                                 .GroupBy(t => t.Id)
                                 .Select(t => t.First()).ToList();
            var defaultTz = tz.First(t => t.Id == offset);
            ViewBag.DefaultTimeZone = defaultTz.Name;
            ViewBag.TimezoneOffsetList = new SelectList(tz, "Id", "Name", defaultTz);
        }
    }
}
