﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;

using Service.Pattern;

using BugFixRocks.Models;
using BugFixRocks.Repository;

namespace BugFixRocks.Web.Controllers
{
    public class IssuesController : ApiController
    {
        private ScaffoldingDBContext db = new ScaffoldingDBContext();


        // GET: api/Issues
        public IQueryable<Issue> GetIssues()
        {
            return db.Issues;
        }

        // GET: api/Issues/5
        [ResponseType(typeof(Issue))]
        public async Task<IHttpActionResult> GetIssue(int id)
        {
            Issue issue = await db.Issues.FindAsync(id);
            if (issue == null)
            {
                return NotFound();
            }

            return Ok(issue);
        }

        // PUT: api/Issues/5
        [ResponseType(typeof(void))]
        public async Task<IHttpActionResult> PutIssue(int id, Issue issue)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            if (id != issue.Id)
            {
                return BadRequest();
            }

            db.Entry(issue).State = EntityState.Modified;

            try
            {
                await db.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!IssueExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return StatusCode(HttpStatusCode.NoContent);
        }

        // POST: api/Issues
        [ResponseType(typeof(Issue))]
        public async Task<IHttpActionResult> PostIssue(Issue issue)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            db.Issues.Add(issue);
            await db.SaveChangesAsync();

            return CreatedAtRoute("DefaultApi", new { id = issue.Id }, issue);
        }

        // DELETE: api/Issues/5
        [ResponseType(typeof(Issue))]
        public async Task<IHttpActionResult> DeleteIssue(int id)
        {
            Issue issue = await db.Issues.FindAsync(id);
            if (issue == null)
            {
                return NotFound();
            }

            db.Issues.Remove(issue);
            await db.SaveChangesAsync();

            return Ok(issue);
        }

        protected override void Dispose(bool disposing)
        {
            if (disposing)
            {
                db.Dispose();
            }
            base.Dispose(disposing);
        }

        private bool IssueExists(int id)
        {
            return db.Issues.Count(e => e.Id == id) > 0;
        }
    }
}