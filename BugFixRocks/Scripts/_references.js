/// <autosync enabled="true" />
/// <reference path="modernizr-2.6.2.js" />
/// <reference path="respond.js" />
/// <reference path="jquery.validate.js" />
/// <reference path="jquery-2.1.3.js" />
/// <reference path="npm.js" />
/// <reference path="bootstrap.min.js" />
/// <reference path="jquery.validate.unobtrusive.min.js" />
/// <reference path="angular-touch.min.js" />
/// <reference path="angular-scenario.js" />
/// <reference path="angular-sanitize.min.js" />
/// <reference path="angular-route.min.js" />
/// <reference path="angular-resource.min.js" />
/// <reference path="angular-mocks.js" />
/// <reference path="angular-messages.min.js" />
/// <reference path="angular-loader.min.js" />
/// <reference path="angular-cookies.min.js" />
/// <reference path="angular-aria.min.js" />
/// <reference path="angular-animate.min.js" />
/// <reference path="angular.min.js" />
/// <reference path="angular-ui/ui-bootstrap-tpls.min.js" />
/// <reference path="angular-ui/ui-bootstrap.js" />
/// <reference path="../app/bugfixrocksapp.js" />
/// <reference path="../app/projectform/controller.js" />
/// <reference path="../app/projectform/directive.js" />
/// <reference path="../app/dataservice.js" />
/// <reference path="../app/validationdirective.js" />
