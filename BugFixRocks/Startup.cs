﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartupAttribute(typeof(BugFixRocks.Web.Startup))]
namespace BugFixRocks.Web
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
        }
    }
}
