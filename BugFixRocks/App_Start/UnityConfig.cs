using System;

using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.Practices.Unity;
using Microsoft.Practices.Unity.Configuration;

using Repository.Pattern.DataContext;
using Repository.Pattern.Ef6;
using Repository.Pattern.UnitOfWork;
using Repository.Pattern.Repositories;
using Service.Pattern;

using BugFixRocks.Models;
using BugFixRocks.Repository;
using BugFixRocks.Web.Controllers;
using BugFixRocks.Services.Interfaces;
using BugFixRocks.Services;

namespace BugFixRocks.Web.App_Start
{
    /// <summary>
    /// Specifies the Unity configuration for the main container.
    /// </summary>
    public class UnityConfig
    {
        #region Unity Container
        private static Lazy<IUnityContainer> container = new Lazy<IUnityContainer>(() =>
        {
            var container = new UnityContainer();
            RegisterTypes(container);
            return container;
        });

        /// <summary>
        /// Gets the configured Unity container.
        /// </summary>
        public static IUnityContainer GetConfiguredContainer()
        {
            return container.Value;
        }
        #endregion

        /// <summary>Registers the type mappings with the Unity container.</summary>
        /// <param name="container">The unity container to configure.</param>
        /// <remarks>There is no need to register concrete types such as controllers or API controllers (unless you want to 
        /// change the defaults), as Unity allows resolving a concrete type even if it was not previously registered.</remarks>
        public static void RegisterTypes(IUnityContainer container)
        {
            // NOTE: To load from web.config uncomment the line below. Make sure to add a Microsoft.Practices.Unity.Configuration to the using statements.
            // container.LoadConfiguration();

            container
                // for authentication
                .RegisterType<AccountController>(new InjectionConstructor())
                .RegisterType<ApplicationDbContext>(new HierarchicalLifetimeManager())
                .RegisterType<IUserStore<ApplicationUser, int>, UserStore<ApplicationUser, ApplicationRole, int, ApplicationUserLogin, ApplicationUserRole, ApplicationUserClaim>>
                    (new HierarchicalLifetimeManager())
                // for UOF
                .RegisterType<IDataContextAsync, BugFixRocksContext>(new PerRequestLifetimeManager())
                .RegisterType<IUnitOfWorkAsync, UnitOfWork>(new PerRequestLifetimeManager())
                // for Model repositories
                .RegisterType<IRepositoryAsync<AspNetRole>, Repository<AspNetRole>>()
                .RegisterType<IRepositoryAsync<AspNetUser>, Repository<AspNetUser>>()
                .RegisterType<IRepositoryAsync<AspNetUserClaim>, Repository<AspNetUserClaim>>()
                .RegisterType<IRepositoryAsync<AspNetUserLogin>, Repository<AspNetUserLogin>>()
                .RegisterType<IRepositoryAsync<Attachment>, Repository<Attachment>>()
                .RegisterType<IRepositoryAsync<EntryType>, Repository<EntryType>>()
                .RegisterType<IRepositoryAsync<Issue>, Repository<Issue>>()
                .RegisterType<IRepositoryAsync<Priority>, Repository<Priority>>()
                .RegisterType<IRepositoryAsync<Project>, Repository<Project>>()
                .RegisterType<IRepositoryAsync<ProjectMember>, Repository<ProjectMember>>()
                .RegisterType<IRepositoryAsync<Resolution>, Repository<Resolution>>()
                .RegisterType<IRepositoryAsync<ResolutionType>, Repository<ResolutionType>>()
                .RegisterType<IRepositoryAsync<Tag>, Repository<Tag>>()
                .RegisterType<IRepositoryAsync<Team>, Repository<Team>>()
                .RegisterType<IRepositoryAsync<TeamMember>, Repository<TeamMember>>()
                .RegisterType<IRepositoryAsync<Versioning>, Repository<Versioning>>()
                // for Service layer
                .RegisterType<ITeamsService, TeamsService>()
            ; // end semi-colon
        }
    }
}
