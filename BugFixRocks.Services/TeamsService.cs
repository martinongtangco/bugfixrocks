﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;

using Repository.Pattern.Repositories;
using Repository.Pattern.Infrastructure;
using Service.Pattern;

using BugFixRocks.Services.Interfaces;
using BugFixRocks.Models;
using BugFixRocks.Services.Utilities;

namespace BugFixRocks.Services
{
    public class TeamsService : Service<Team>, ITeamsService
    {
        private readonly IRepositoryAsync<Team> _repo;

        public TeamsService(IRepositoryAsync<Team> repo)
            : base(repo)
        {
            _repo = repo;
        }

        public async Task<IEnumerable<Team>> GetAllTeams()
        {
            return await _repo.Queryable().IncludeMap("Creator").ToListAsync();
        }

        public async Task Update(Team team)
        {
            Team existingTeam = await _repo.FindAsync(team.Id);
            if (existingTeam == null)
            {
                throw new ArgumentNullException();
            }

            // changes
            existingTeam.ObjectState = ObjectState.Modified;
            existingTeam.ModifiedUTC = DateTime.UtcNow;
            existingTeam.Name = team.Name;
            existingTeam.LogoBase64 = team.LogoBase64;
            existingTeam.Description = team.Description;
            existingTeam.Email = team.Email;
            existingTeam.Website = team.Website;
            existingTeam.Timezone = team.Timezone;

            _repo.Update(existingTeam);
        }

        public void Insert(Team team, int userId)
        {
            if (userId < 1)
            {
                throw new AccessViolationException("userId must exist!");
            }

            team.CreatorId = userId;
            team.CreatedUTC = DateTime.UtcNow;
            _repo.Insert(team);
        }
    }
}
