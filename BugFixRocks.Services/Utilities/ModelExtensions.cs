﻿using System;
using System.Collections.Generic;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace BugFixRocks.Services.Utilities
{
    public static class ModelExtensions
    {
        public static IQueryable<T> IncludeMap<T>
                (this IQueryable<T> sequence, string path) where T : class
        {
            var dbQuery = sequence as DbQuery<T>;
            if (dbQuery != null)
            {
                var queryInclude = dbQuery.Include(path);
                if (queryInclude != null)
                    return queryInclude;
            }

            return sequence;
        }
    }
}
