﻿using System;
using System.Threading.Tasks;
using System.Collections.Generic;

using Service.Pattern;

using BugFixRocks.Models;

namespace BugFixRocks.Services.Interfaces
{
    public interface ITeamsService : IService<Team>
    {
        Task<IEnumerable<Team>> GetAllTeams();
        Task Update(Team team);
        void Insert(Team team, int userId);
    }
}
