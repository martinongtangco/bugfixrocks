﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Service.Pattern;

using BugFixRocks.Models;

namespace BugFixRocks.Services.Interfaces
{
    public interface IIssuesService : IService<Issue>
    {
        Task<IEnumerable<Issue>> GetAllIssues();
        Task<IEnumerable<Issue>> GetIssueBySeverity(int severityId);
    }
}
