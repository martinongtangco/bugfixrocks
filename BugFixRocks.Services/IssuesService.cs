﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;

using Repository.Pattern.Repositories;
using Service.Pattern;

using BugFixRocks.Services.Interfaces;
using BugFixRocks.Models;

namespace BugFixRocks.Services
{
    public class IssuesService : Service<Issue>, IIssuesService
    {
        private readonly IRepositoryAsync<Issue> _repo;
        
        public IssuesService(IRepositoryAsync<Issue> repo)
            : base(repo)
        {
            _repo = repo;
        }

        public async Task<IEnumerable<Issue>> GetAllIssues()
        {
            return await _repo.Queryable().ToListAsync();
        }

        public async Task<IEnumerable<Issue>> GetIssueBySeverity(int severityId)
        {
            return await _repo.Queryable().ToListAsync();
        }
    }
}
