# BugFixRocks README #

This project is solely for educational purposes only. This project demonstrates the proper use of the Generic Unit of Work & Repository pattern framework, a modified authentication EF model that uses integer primary Ids instead of the built-in GUID primary Ids, OAuth2, and AngularJS.

This is still a work in progress.

### What is this repository for? ###

* Quick summary
* Version 0.2
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Summary of set up
* Configuration
* Dependencies
* Database configuration
* How to run tests
* Deployment instructions

### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines

### Who do I talk to? ###

* Contact me, Martin Ongtangco for more information
* Other community or team contact