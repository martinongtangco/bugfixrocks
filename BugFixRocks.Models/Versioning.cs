using System;
using System.Collections.Generic;
using Repository.Pattern.Ef6;

namespace BugFixRocks.Models
{
    public partial class Versioning : Entity
    {
        public int Id { get; set; }
        public int ProjectId { get; set; }
        public string Version { get; set; }
        public System.DateTime CreatedUTC { get; set; }
        public Nullable<System.DateTime> ModifiedUTC { get; set; }
        public int CreatorId { get; set; }
        public virtual Project Project { get; set; }
    }
}
