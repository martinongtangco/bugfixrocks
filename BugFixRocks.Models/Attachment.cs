using System;
using System.Collections.Generic;
using Repository.Pattern.Ef6;
using System.ComponentModel.DataAnnotations;

namespace BugFixRocks.Models
{
    public partial class Attachment : Entity
    {
        [Key]
        public int IssueId { get; set; }
        public string AttachmentURL { get; set; }
        public virtual Issue Issue { get; set; }
    }
}
