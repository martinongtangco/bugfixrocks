using System;
using System.Collections.Generic;
using Repository.Pattern.Ef6;
using System.ComponentModel.DataAnnotations;

namespace BugFixRocks.Models
{
    public partial class AspNetUserLogin : Entity
    {
        public string LoginProvider { get; set; }
        public string ProviderKey { get; set; }
        [Key]
        public int UserId { get; set; }
        public virtual AspNetUser AspNetUser { get; set; }
    }
}
