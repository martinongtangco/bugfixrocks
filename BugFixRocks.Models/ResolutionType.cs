using System;
using System.Collections.Generic;
using Repository.Pattern.Ef6;

namespace BugFixRocks.Models
{
    public partial class ResolutionType : Entity
    {
        public ResolutionType()
        {
            this.Issues = new List<Issue>();
            this.Resolutions = new List<Resolution>();
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public virtual ICollection<Issue> Issues { get; set; }
        public virtual ICollection<Resolution> Resolutions { get; set; }
    }
}
