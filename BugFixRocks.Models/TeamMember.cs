using System;
using System.Collections.Generic;
using Repository.Pattern.Ef6;

namespace BugFixRocks.Models
{
    public partial class TeamMember : Entity
    {
        public int Id { get; set; }
        public int ProjectId { get; set; }
        public int UserId { get; set; }
        public virtual Project Project { get; set; }
    }
}
