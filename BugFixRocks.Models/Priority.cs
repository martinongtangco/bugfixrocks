using System;
using System.Collections.Generic;
using Repository.Pattern.Ef6;

namespace BugFixRocks.Models
{
    public partial class Priority : Entity
    {
        public Priority()
        {
            this.Issues = new List<Issue>();
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public virtual ICollection<Issue> Issues { get; set; }
    }
}
