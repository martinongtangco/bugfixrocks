using System;
using System.Collections.Generic;
using Repository.Pattern.Ef6;

namespace BugFixRocks.Models
{
    public partial class Resolution : Entity
    {
        public int Id { get; set; }
        public int IssueId { get; set; }
        public int ResolutionType { get; set; }
        public string Comment { get; set; }
        public int UserId { get; set; }
        public System.DateTime CreatedUTC { get; set; }
        public virtual Issue Issue { get; set; }
        public virtual ResolutionType ResolutionType1 { get; set; }
    }
}
