using System;
using System.Collections.Generic;
using Repository.Pattern.Ef6;

namespace BugFixRocks.Models
{
    public partial class ProjectMember : Entity
    {
        public int Id { get; set; }
        public int ProjectId { get; set; }
        public int UserId { get; set; }
        public bool NotifyUpdates { get; set; }
        public string NotifyDays { get; set; }
        public short NotifyStartHour { get; set; }
        public short NotifyEndHour { get; set; }
        public short Timezone { get; set; }
        public System.DateTime CreatedUTC { get; set; }
        public Nullable<System.DateTime> ModifiedUTC { get; set; }
        public virtual Project Project { get; set; }
    }
}
