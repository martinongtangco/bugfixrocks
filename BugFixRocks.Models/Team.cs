using System;
using System.Collections.Generic;
using Repository.Pattern.Ef6;
using System.ComponentModel.DataAnnotations;

namespace BugFixRocks.Models
{
    public partial class Team : Entity
    {
        public Team()
        {
            this.Tags = new List<Tag>();
        }

        public int Id { get; set; }
        
        [Required]
        [MaxLength(100)]
        public string Name { get; set; }

        [Display(Name = "Avatar")]
        public string LogoBase64 { get; set; }

        [MaxLength(1000)]
        public string Description { get; set; }
        
        public int CreatorId { get; set; }

        [Display(Name = "Created")]
        [DataType(DataType.DateTime)]
        public DateTime CreatedUTC { get; set; }
        
        [Display(Name = "Last Modified")]
        [DataType(DataType.DateTime)]
        public DateTime? ModifiedUTC { get; set; }
        
        [Display(Name = "Is Public")]
        public bool IsPublic { get; set; }

        [Required]
        [DataType(DataType.EmailAddress)]
        [MaxLength(2000)]
        public string Email { get; set; }

        [DataType(DataType.Url)]
        [MaxLength(2000)]
        public string Website { get; set; }
        
        [Display(Name = "Email Group")]
        public bool EmailGroup { get; set; }
        
        [Display(Name = "Timezone Offset")]
        public short Timezone { get; set; }

        public virtual ICollection<Tag> Tags { get; set; }
        public virtual AspNetUser Creator { get; set; }
    }
}
