using System;
using System.Collections.Generic;
using Repository.Pattern.Ef6;

namespace BugFixRocks.Models
{
    public partial class AspNetUserClaim : Entity
    {
        public int Id { get; set; }
        public int UserId { get; set; }
        public string ClaimType { get; set; }
        public string ClaimValue { get; set; }
        public virtual AspNetUser AspNetUser { get; set; }
    }
}
