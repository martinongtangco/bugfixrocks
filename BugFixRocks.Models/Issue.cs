using System;
using System.Collections.Generic;
using System.Web.Mvc;
using System.ComponentModel.DataAnnotations;

using Repository.Pattern.Ef6;

namespace BugFixRocks.Models
{
    public partial class Issue : Entity
    {
        public Issue()
        {
            this.Attachments = new List<Attachment>();
            this.Resolutions = new List<Resolution>();
        }

        [HiddenInput(DisplayValue = true)]
        public int Id { get; set; }

        [HiddenInput(DisplayValue = false)]
        public int ProjectId { get; set; }

        [HiddenInput(DisplayValue = false)]
        public Nullable<int> VersionId { get; set; }
        
        public string Title { get; set; }

        public string Description { get; set; }

        [HiddenInput(DisplayValue = false)]
        public int PriorityId { get; set; }

        [HiddenInput(DisplayValue = false)]
        public int ResolutionTypeId { get; set; }

        [HiddenInput(DisplayValue = false)]
        public Nullable<int> AssigneeId { get; set; }

        [HiddenInput(DisplayValue = false)]
        public int ReporterId { get; set; }
        
        [Display(Name = "Date Created")]
        public System.DateTime CreatedUTC { get; set; }

        [Display(Name = "Date Modified")]
        public Nullable<System.DateTime> ModifiedUTC { get; set; }

        public string Tags { get; set; }
        
        public virtual Project Project { get; set; }
        public virtual ICollection<Attachment> Attachments { get; set; }
        public virtual Priority Priority { get; set; }
        public virtual ResolutionType ResolutionType { get; set; }
        public virtual ICollection<Resolution> Resolutions { get; set; }
    }
}
