using System;
using System.Collections.Generic;
using Repository.Pattern.Ef6;

namespace BugFixRocks.Models
{
    public partial class Tag : Entity
    {
        public int Id { get; set; }
        public int TeamId { get; set; }
        public string Tag1 { get; set; }
        public virtual Team Team { get; set; }
    }
}
