using System;
using System.Collections.Generic;
using Repository.Pattern.Ef6;

namespace BugFixRocks.Models
{
    public partial class Project : Entity
    {
        public Project()
        {
            this.ProjectMembers = new List<ProjectMember>();
            this.Issues = new List<Issue>();
            this.TeamMembers = new List<TeamMember>();
            this.Versionings = new List<Versioning>();
        }

        public int Id { get; set; }
        public string Name { get; set; }
        public System.DateTime CreatedUTC { get; set; }
        public Nullable<System.DateTime> ModifiedUTC { get; set; }
        public int CreatorId { get; set; }
        public string LogoBase64 { get; set; }
        public virtual ICollection<ProjectMember> ProjectMembers { get; set; }
        public virtual ICollection<Issue> Issues { get; set; }
        public virtual ICollection<TeamMember> TeamMembers { get; set; }
        public virtual ICollection<Versioning> Versionings { get; set; }
    }
}
