using System;
using System.Collections.Generic;
using Repository.Pattern.Ef6;

namespace BugFixRocks.Models
{
    public partial class EntryType : Entity
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
