﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Moq;

using Repository.Pattern.Ef6;

using BugFixRocks.Tests.Stubs;

namespace BugFixRocks.Tests.Utilities
{
    public static class GenericMockBuilders
    {
        public static Mock<DbSet<TEnt>> GenerateMockDBSet<TEnt>(this IQueryable<TEnt> data)
            where TEnt : Entity
        {
            var mockSet = new Mock<DbSet<TEnt>>();
            mockSet.As<IDbAsyncEnumerable<TEnt>>()
                .Setup(m => m.GetAsyncEnumerator())
                .Returns(new TestDbAsyncEnumerator<TEnt>(data.GetEnumerator()));

            mockSet.As<IQueryable<TEnt>>()
                .Setup(m => m.Provider)
                .Returns(new TestDbAsyncQueryProvider<TEnt>(data.Provider));

            mockSet.As<IQueryable<TEnt>>().Setup(m => m.Expression).Returns(data.Expression);
            mockSet.As<IQueryable<TEnt>>().Setup(m => m.ElementType).Returns(data.ElementType);
            mockSet.As<IQueryable<TEnt>>().Setup(m => m.Provider).Returns(data.Provider);
            mockSet.As<IQueryable<TEnt>>().Setup(m => m.GetEnumerator()).Returns(data.GetEnumerator);

            return mockSet;
        }

        public static void SetupIQueryable<T>(this Mock<T> mock, IQueryable queryable)
            where T : class, IQueryable
        {
            mock.Setup(r => r.GetEnumerator()).Returns(queryable.GetEnumerator);
            mock.Setup(r => r.Provider).Returns(queryable.Provider);
            mock.Setup(r => r.ElementType).Returns(queryable.ElementType);
            mock.Setup(r => r.Expression).Returns(queryable.Expression);
        }
    }
}
