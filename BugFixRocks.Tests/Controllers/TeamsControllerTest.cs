﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Mvc;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Net;
using System.Security.Claims;
using System.Security.Principal;

using Microsoft.VisualStudio.TestTools.UnitTesting;

using Moq;

using Repository.Pattern.UnitOfWork;
using Repository.Pattern.Ef6;
using Repository.Pattern.DataContext;
using Repository.Pattern.Repositories;

using BugFixRocks.Models;
using BugFixRocks.Web.Controllers;
using BugFixRocks.Tests.Utilities;
using BugFixRocks.Repository.Contexts;
using BugFixRocks.Services.Interfaces;
using BugFixRocks.Services;

namespace BugFixRocks.Tests.Controllers
{
    [TestClass]
    public class TeamsControllerTest
    {
        private Mock<IUnitOfWorkAsync> _uow;
        private Mock<IRepositoryAsync<Team>> _repo;
        private Mock<ControllerContext> _controllerContext;
        private Mock<IDataContextAsync> _dataContextAsync;
        private ITeamsService _teamsService;
        private ControllerContext _contContext;

        private TeamsController _controller;

        [TestInitialize]
        public void StartUp()
        {
            _uow = new Mock<IUnitOfWorkAsync>();
            _repo = new Mock<IRepositoryAsync<Team>>();
            _dataContextAsync = new Mock<IDataContextAsync>();
            
            // simulate authenticated user            
            var claim = new Claim("Admin", "1");
            var mockIdentity =
                Mock.Of<ClaimsIdentity>(ci => ci.FindFirst(It.IsAny<string>()) == claim);

            _controllerContext = new Mock<ControllerContext>();
            _controllerContext.SetupGet(x => x.HttpContext.User.Identity.Name).Returns("Admin");
            _controllerContext.SetupGet(x => x.HttpContext.Request.IsAuthenticated).Returns(true);
            _controllerContext.SetupGet(x => x.HttpContext.User).Returns(Mock.Of<IPrincipal>(ip => ip.Identity == mockIdentity));
        }

        [TestCleanup]
        public void CleanUp()
        {

        }

        [TestMethod]
        public async Task Index_AccessIndexPage_MustPass()
        {
            // arrange
            var data = new List<Team> 
            { 
                new Team { Id = 1 },
                new Team { Id = 2 }
            }.AsQueryable();

            Mock<DbSet<Team>> mockSet = data.GenerateMockDBSet<Team>();
            _dataContextAsync.As<IBugFixRocksContext>().Setup(c => c.Teams).Returns(mockSet.Object);
            _repo.Setup(r => r.Queryable()).Returns(_dataContextAsync.As<IBugFixRocksContext>().Object.Teams.AsQueryable());
            _uow.Setup(u => u.RepositoryAsync<Team>()).Returns(_repo.Object);
            _teamsService = new TeamsService(_repo.Object);

            _controller = new TeamsController(_uow.Object, _teamsService);
            _controller.ControllerContext = _controllerContext.Object;

            // act
            var result = await _controller.Index();
            var model = (List<Team>)((ViewResult)result).Model;

            // assert
            Assert.IsNotNull(model);
            Assert.AreEqual(model.Count, 2);

            // verify mock
            _dataContextAsync.As<IBugFixRocksContext>().VerifyGet(a => a.Teams);
            _repo.Verify(r => r.Queryable());
        }

        [TestMethod]
        public async Task Details_AccessDetailsPageWithNullTeamId_MustFail()
        {
            // arrange
            var data = new List<Team> 
            { 
                new Team { Id = 1 }
            }.AsQueryable();

            Mock<DbSet<Team>> mockSet = data.GenerateMockDBSet<Team>();
            _dataContextAsync.As<IBugFixRocksContext>().Setup(c => c.Teams).Returns(mockSet.Object);
            _repo.Setup(r => r.FindAsync(It.IsAny<int>()))
                 .ReturnsAsync(null);
            _uow.Setup(u => u.RepositoryAsync<Team>()).Returns(_repo.Object);
            _teamsService = new TeamsService(_repo.Object);

            _controller = new TeamsController(_uow.Object, _teamsService);
            _controller.ControllerContext = _controllerContext.Object;

            // act
            int? testId = null;
            dynamic result = await _controller.Details(testId);

            // assert
            Assert.IsInstanceOfType(result, typeof(HttpStatusCodeResult));
            var httpResult = result as HttpStatusCodeResult;
            Assert.AreEqual(HttpStatusCode.BadRequest, (HttpStatusCode)httpResult.StatusCode);
        }

        [TestMethod]
        public async Task Details_AccessDetailsPageWithInvalidTeamId_MustFail()
        {
            // arrange
            var data = new List<Team> 
            { 
                new Team { Id = 1 }
            }.AsQueryable();

            Mock<DbSet<Team>> mockSet = data.GenerateMockDBSet<Team>();
            _dataContextAsync.As<IBugFixRocksContext>().Setup(c => c.Teams).Returns(mockSet.Object);
            _repo.Setup(r => r.FindAsync(It.IsAny<int>()))
                 .ReturnsAsync(null);
            _uow.Setup(u => u.RepositoryAsync<Team>()).Returns(_repo.Object);
            _teamsService = new TeamsService(_repo.Object);

            _controller = new TeamsController(_uow.Object, _teamsService);
            _controller.ControllerContext = _controllerContext.Object;

            // act
            int? testId = 2;
            dynamic result = await _controller.Details(testId);

            // assert
            Assert.IsInstanceOfType(result, typeof(HttpStatusCodeResult));
            var httpResult = result as HttpStatusCodeResult;
            Assert.AreEqual(HttpStatusCode.NotFound, (HttpStatusCode)httpResult.StatusCode);
        }

        [TestMethod]
        public async Task Details_AccessDetailsPageWithValidTeamId_MustPass()
        {
            // arrange
            var data = new List<Team> 
            { 
                new Team { Id = 1 }
            }.AsQueryable();

            Mock<DbSet<Team>> mockSet = data.GenerateMockDBSet<Team>();
            _dataContextAsync.As<IBugFixRocksContext>().Setup(c => c.Teams).Returns(mockSet.Object);
            _repo.Setup(r => r.FindAsync(It.IsAny<int>()))
                 .ReturnsAsync(_dataContextAsync.As<IBugFixRocksContext>().Object.Teams.First());
            _uow.Setup(u => u.RepositoryAsync<Team>()).Returns(_repo.Object);
            _teamsService = new TeamsService(_repo.Object);

            _controller = new TeamsController(_uow.Object, _teamsService);
            _controller.ControllerContext = _controllerContext.Object;

            // act
            int? testId = 1;
            dynamic result = await _controller.Details(testId);
            var model = result.Model;

            // assert
            Assert.IsNotNull(model);
            Assert.AreEqual(model.Id, testId);

            // verify mock
            _dataContextAsync.As<IBugFixRocksContext>().VerifyGet(a => a.Teams);
            _repo.Verify(r => r.FindAsync(It.IsAny<int>()));
        }

        [TestMethod]
        public async Task Create_RaiseInsertException_MustFail()
        {
            // arrange
            Team testData = new Team 
            {
                Name = string.Empty // Name is required field
            };

            Mock<DbSet<Team>> mockSet = (new List<Team>()).AsQueryable().GenerateMockDBSet<Team>();
            _dataContextAsync.As<IBugFixRocksContext>().Setup(c => c.Teams).Returns(mockSet.Object);
            _uow.Setup(u => u.RepositoryAsync<Team>()).Returns(_repo.Object);
            _uow.Setup(u => u.SaveChangesAsync()).Throws(new Exception()); // insert fail simulation
            _teamsService = new TeamsService(_repo.Object);

            _controller = new TeamsController(_uow.Object, _teamsService);
            _controller.ControllerContext = _controllerContext.Object;

            // act
            dynamic result = await _controller.Create(testData);

            // assert
            Assert.IsInstanceOfType(result, typeof(HttpStatusCodeResult));
            var httpResult = result as HttpStatusCodeResult;
            Assert.AreEqual(HttpStatusCode.BadRequest, (HttpStatusCode)httpResult.StatusCode);

            // verify mock
            _uow.Verify(u => u.SaveChangesAsync());
            _uow.Verify(u => u.Rollback());
        }

        [TestMethod]
        public async Task Create_InsertValidTeam_MustPass()
        {
            Team testData = new Team
            {
                Name = "New Team"
            };

            Mock<DbSet<Team>> mockSet = (new List<Team>()).AsQueryable().GenerateMockDBSet<Team>();
            _dataContextAsync.As<IBugFixRocksContext>().Setup(c => c.Teams).Returns(mockSet.Object);
            _uow.Setup(u => u.RepositoryAsync<Team>()).Returns(_repo.Object);
            _teamsService = new TeamsService(_repo.Object);

            _controller = new TeamsController(_uow.Object, _teamsService);
            _controller.ControllerContext = _controllerContext.Object;

            // act
            dynamic result = await _controller.Create(testData);

            // assert
            // verify mock
            _uow.Verify(u => u.SaveChangesAsync());
            _uow.Verify(u => u.Commit());
        }

        [TestMethod]
        public async Task Edit_PassInvalidTeamId_MustFail()
        {
            // arrange
            Team testData = new Team
            {
                Name = string.Empty // Name is required field
            };

            Mock<DbSet<Team>> mockSet = (new List<Team>()).AsQueryable().GenerateMockDBSet<Team>();
            _dataContextAsync.As<IBugFixRocksContext>().Setup(c => c.Teams).Returns(mockSet.Object);
            _uow.Setup(u => u.RepositoryAsync<Team>()).Returns(_repo.Object);
            _teamsService = new TeamsService(_repo.Object);

            _controller = new TeamsController(_uow.Object, _teamsService);
            _controller.ControllerContext = _controllerContext.Object;

            // act
            dynamic result = await _controller.Edit(testData);

            // assert
            Assert.IsInstanceOfType(result, typeof(HttpStatusCodeResult));
            var httpResult = result as HttpStatusCodeResult;
            Assert.AreEqual(HttpStatusCode.BadRequest, (HttpStatusCode)httpResult.StatusCode);
        }

        [TestMethod]
        public async Task Edit_InsertValidTeam_MustPass()
        {
            Team testData = new Team
            {
                Name = "New Team"
            };

            Mock<DbSet<Team>> mockSet = (new List<Team>() { new Team { Id = 1 } }).AsQueryable().GenerateMockDBSet<Team>();
            _dataContextAsync.As<IBugFixRocksContext>().Setup(c => c.Teams).Returns(mockSet.Object);
            _repo.Setup(r => r.FindAsync(It.IsAny<int>()))
                 .ReturnsAsync(mockSet.Object.First());
            _uow.Setup(u => u.RepositoryAsync<Team>()).Returns(_repo.Object);
            _teamsService = new TeamsService(_repo.Object);

            _controller = new TeamsController(_uow.Object, _teamsService);
            _controller.ControllerContext = _controllerContext.Object;

            // act
            dynamic result = await _controller.Edit(testData);

            // assert
            // verify mock
            _uow.Verify(u => u.SaveChangesAsync());
            _uow.Verify(u => u.Commit());
        }

        [TestMethod]
        public async Task Delete_PassNullId_MustFail()
        {
            int? testId = null;

            Mock<DbSet<Team>> mockSet = (new List<Team>()).AsQueryable().GenerateMockDBSet<Team>();
            _dataContextAsync.As<IBugFixRocksContext>().Setup(c => c.Teams).Returns(mockSet.Object);
            _uow.Setup(u => u.RepositoryAsync<Team>()).Returns(_repo.Object);
            _teamsService = new TeamsService(_repo.Object);

            _controller = new TeamsController(_uow.Object, _teamsService);
            _controller.ControllerContext = _controllerContext.Object;

            // act
            dynamic result = await _controller.Delete(testId);

            // assert
            Assert.IsInstanceOfType(result, typeof(HttpStatusCodeResult));
            var httpResult = result as HttpStatusCodeResult;
            Assert.AreEqual(HttpStatusCode.BadRequest, (HttpStatusCode)httpResult.StatusCode);
        }

        [TestMethod]
        public async Task Delete_PassInvalidId_MustFail()
        {
            int? testId = 1; // invalid id

            Mock<DbSet<Team>> mockSet = (new List<Team>()).AsQueryable().GenerateMockDBSet<Team>();
            _dataContextAsync.As<IBugFixRocksContext>().Setup(c => c.Teams).Returns(mockSet.Object);
            _repo.Setup(r => r.FindAsync(It.IsAny<int>())).ReturnsAsync(null);
            _uow.Setup(u => u.RepositoryAsync<Team>()).Returns(_repo.Object);
            _teamsService = new TeamsService(_repo.Object);

            _controller = new TeamsController(_uow.Object, _teamsService);
            _controller.ControllerContext = _controllerContext.Object;

            // act
            dynamic result = await _controller.Delete(testId);

            // assert
            Assert.IsInstanceOfType(result, typeof(HttpStatusCodeResult));
            var httpResult = result as HttpStatusCodeResult;
            Assert.AreEqual(HttpStatusCode.NotFound, (HttpStatusCode)httpResult.StatusCode);
        }

        [TestMethod]
        public async Task Delete_PassValidId_MustPass()
        {
            int? testId = 1; // invalid id

            Mock<DbSet<Team>> mockSet = (new List<Team> { new Team { Id = 1 } }).AsQueryable().GenerateMockDBSet<Team>();
            _dataContextAsync.As<IBugFixRocksContext>().Setup(c => c.Teams).Returns(mockSet.Object);
            _repo.Setup(r => r.FindAsync(It.IsAny<int>())).ReturnsAsync(_dataContextAsync.As<IBugFixRocksContext>().Object.Teams.First());
            _uow.Setup(u => u.RepositoryAsync<Team>()).Returns(_repo.Object);
            _teamsService = new TeamsService(_repo.Object);

            _controller = new TeamsController(_uow.Object, _teamsService);
            _controller.ControllerContext = _controllerContext.Object;

            // act
            dynamic result = await _controller.Delete(testId);
            Team model = result.Model;

            // assert
            Assert.IsNotNull(model);
            _repo.Verify(r => r.FindAsync(It.IsAny<int>()));
        }

        [TestMethod]
        public async Task DeleteConfirmed_RaiseDeleteException_MustFail()
        {
            // arrange
            int testId = 1;

            Mock<DbSet<Team>> mockSet = (new List<Team>()).AsQueryable().GenerateMockDBSet<Team>();
            _dataContextAsync.As<IBugFixRocksContext>().Setup(c => c.Teams).Returns(mockSet.Object);
            _uow.Setup(u => u.RepositoryAsync<Team>()).Returns(_repo.Object);
            _uow.Setup(u => u.SaveChangesAsync()).Throws(new Exception()); // insert fail simulation
            _teamsService = new TeamsService(_repo.Object);

            _controller = new TeamsController(_uow.Object, _teamsService);
            _controller.ControllerContext = _controllerContext.Object;

            // act
            dynamic result = await _controller.DeleteConfirmed(testId);

            // assert
            Assert.IsInstanceOfType(result, typeof(HttpStatusCodeResult));
            var httpResult = result as HttpStatusCodeResult;
            Assert.AreEqual(HttpStatusCode.BadRequest, (HttpStatusCode)httpResult.StatusCode);

            // verify mock
            _uow.Verify(u => u.SaveChangesAsync());
            _uow.Verify(u => u.Rollback());
        }

        [TestMethod]
        public async Task DeleteConfirmed_PassValidId_MustPass()
        {
            // arrange
            int testId = 1;

            Mock<DbSet<Team>> mockSet = (new List<Team>()).AsQueryable().GenerateMockDBSet<Team>();
            _dataContextAsync.As<IBugFixRocksContext>().Setup(c => c.Teams).Returns(mockSet.Object);
            _uow.Setup(u => u.RepositoryAsync<Team>()).Returns(_repo.Object);
            _teamsService = new TeamsService(_repo.Object);

            _controller = new TeamsController(_uow.Object, _teamsService);
            _controller.ControllerContext = _controllerContext.Object;

            // act
            dynamic result = await _controller.DeleteConfirmed(testId);

            // assert
            // verify mock
            _uow.Verify(u => u.SaveChangesAsync());
            _uow.Verify(u => u.Commit());
        }
    }
}
